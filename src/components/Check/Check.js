import React, {useState} from 'react';
import Individual from "../Individual/Individual";
import Split from "../ForAll/Split";

const Check = () => {
    const [mode, setMode] = useState('split');

    const onRadioChange = e => {
        setMode(e.target.value)
    };

    return (
        <div>
            <div>
                <label>
                    <input
                        checked={mode === 'split'}
                        type="radio"
                        name="mode"
                        value="split"
                        onChange={onRadioChange}
                    /> Поровну между участниками
                </label>
            </div>
            <div>
                <label>
                    <input
                        checked={mode === 'individual'}
                        type="radio"
                        name="mode"
                        value="individual"
                        onChange={onRadioChange}
                    /> Каждому индивидуально
                </label>
            </div>
            {mode === 'split' ? (<Split/>) : (<Individual/>)}
        </div>
    );
};

export default Check;