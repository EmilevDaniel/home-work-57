import React, {useState} from 'react';

const Individual = () => {
    const [people, setPeople] = useState([]);
    const [indCheck, setIndCheck] = useState([]);
    const [tips, setTips] = useState({
        tips: 0,
        delivery: 0,
    });

    const changeTips = (name, value) => {
        setTips({...tips, [name]: value})
    };

    const addPerson = () => {
        setPeople(people => [...people, {name: '', price: '', id: Math.random()}])
    };

    const changePersonField = (id, name, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, [name]: value}
                }
                return person;
            })
        })
    };

    const changePriceField = (id, price, value) => {
        setPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, [price]: value}
                }
                return person;
            })
        })
    };

    const makeIndCheck = () => {
        const sum = people.map(p => parseInt(p.price)).reduce((acc, p) => p + acc);
        setIndCheck(
            <div>
                <p>Общая сумма: {sum + (sum * parseInt(tips.tips) / 100) + parseInt(tips.delivery)}</p>
                {people.map(p => {
                    let indSum = Math.ceil((parseInt(p.price) + (parseInt(p.price) * parseInt(tips.tips) / 100)) + (parseInt(tips.delivery) / Object.keys(people).length));
                    return (<p>{p.name}: {indSum}</p>)
                })}
            </div>
        )
    };

    return (
        <div>
            <form>
                {people.map(person => (
                    <div key={person.id}>
                        <input
                            type="text"
                            placeholder="Name"
                            value={person.name}
                            onChange={e => changePersonField(person.id, 'name', e.target.value)}
                        />
                        <label>
                            <input
                                type="number"
                                placeholder="Sum"
                                onChange={e => changePriceField(person.id, 'price', e.target.value)}
                            /> сом
                        </label>

                        <button type="button">Delete</button>
                    </div>
                ))}
                <button onClick={addPerson} type='button'>Add new person</button>
                <div>
                    <label>
                        Процент чаевых:
                        <input
                            type="number"
                            placeholder='%'
                            onChange={e => changeTips('tips', parseInt(e.target.value))}
                        /> %
                    </label>
                </div>
                <div>
                    <label>
                        Доставка:
                        <input
                            type="number"
                            placeholder='0'
                            onChange={e => changeTips('delivery', parseInt(e.target.value))}
                        /> сом
                    </label>
                </div>
                <button onClick={() => makeIndCheck()} type="button">Расчитать</button>
            </form>
            {indCheck}
        </div>
    );
};

export default Individual;