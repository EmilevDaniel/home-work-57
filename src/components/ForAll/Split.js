import React, {useState} from 'react';

const Split = () => {
    const [split, setSplit] = useState({
            numberOfPeople: 0,
            sum: 0,
            tips: 0,
            delivery: 0,
        }
    );
    const [check, setCheck] = useState([]);

    const sumWithTips = Math.ceil(split.sum + (split.sum * split.tips / 100) + split.delivery);
    const splitCheck = Math.ceil((split.sum + (split.sum * split.tips / 100) + split.delivery) / split.numberOfPeople);

    const changeState = (name, value) => {
        setSplit({...split, [name]: value})
    };

    const makeCheck = () => {
        setCheck(
            <div>
                <p>Общая сумма: {sumWithTips}</p>
                <p>Количество человек: {split.numberOfPeople}</p>
                <p>Каждый
                    платит: {splitCheck}</p>
            </div>
        )
    };

    return (
        <div>
            <form>
                <div>
                    <label> Человек:
                        <input type="number"
                               onChange={e => changeState('numberOfPeople', parseInt(e.target.value))}
                        />
                        чел.
                    </label>
                </div>
                <div>
                    <label> Сумма заказа:
                        <input type="number"
                               onChange={e => changeState('sum', parseInt(e.target.value))}
                        />
                        сом
                    </label>
                </div>
                <div>
                    <label> Процент чаевых:
                        <input type="number"
                               onChange={e => changeState('tips', parseInt(e.target.value))}
                        />
                        %
                    </label>
                </div>
                <div>
                    <label> Доставка:
                        <input type="number"
                               onChange={e => changeState('delivery', parseInt(e.target.value))}
                        />
                        сом
                    </label>
                </div>
                <button onClick={() => makeCheck()} type='button'>Расчитать</button>
            </form>
            {check}
        </div>
    );
};

export default Split;